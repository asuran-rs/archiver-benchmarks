from statistics import stdev, mean
import tempfile
import shutil
import pathlib
from pathlib import Path


class Results:
    min_time = 0.0
    avg_time = 0.0
    max_time = 0.0
    std_dev = 0.0

    def __init__(self, min_time, avg_time, max_time, std_dev):
        self.min_time = min_time
        self.avg_time = avg_time
        self.max_time = max_time
        self.std_dev = std_dev

    def __str__(self):
        return "min: {:0.2f}s avg: {:0.2f}s max: {:0.2f}s std: {:0.2f}s".format(self.min_time,
                                                                                self.avg_time,
                                                                                self.max_time,
                                                                                self.std_dev)

    def __repr__(self):
        return str(self)

    def to_row(self, name):
        return [name,
                "{:0.2f}s".format(self.min_time),
                "{:0.2f}s".format(self.avg_time),
                "{:0.2f}s".format(self.max_time),
                "{:0.2f}s".format(self.std_dev)]


class Harness:
    """Testing Harness for Deduplicating Archivers"""

    def __init__(self, count, shim):
        self.shim = shim
        self.count = count

    def name(self):
        return self.shim.name()

    def version(self):
        return self.shim.version()

    def new(self):
        directory = tempfile.TemporaryDirectory()
        path = directory.name + "/repo"
        times = []
        for i in range(0, self.count):
            times.append(self.shim.new(path))
            shutil.rmtree(path)
        min_time = min(times)
        max_time = max(times)
        average = mean(times)
        std_dev = stdev(times)
        return Results(min_time, average, max_time, std_dev).to_row(self.name())

    def snapshot_no_compression(self, data_path):
        repo_dir = tempfile.TemporaryDirectory()
        repo_path = repo_dir.name + "/repo"
        times = []
        sizes = []
        for i in range(0, self.count):
            times.append(self.shim.snapshot_no_compression(
                repo_path, data_path))
            root_directory = Path(repo_path)
            sizes.append(sum(f.stat().st_size for f in root_directory.glob(
                '**/*') if f.is_file())/10**6)
            shutil.rmtree(repo_path)
        (min_time, max_time, avg_time, std_time) = (
            min(times), max(times), mean(times), stdev(times))
        (min_size, max_size, avg_size, std_size) = (
            min(sizes), max(sizes), mean(sizes), stdev(sizes))

        ret = Results(min_time, avg_time, max_time,
                      std_time).to_row(self.name())
        ret.extend(["{:0.2f}".format(avg_size), "{:0.2f}".format(max_size)])
        return ret

    def snapshot_zstd_1(self, data_path):
        repo_dir = tempfile.TemporaryDirectory()
        repo_path = repo_dir.name + "/repo"
        times = []
        sizes = []
        for i in range(0, self.count):
            times.append(self.shim.snapshot_zstd_1(
                repo_path, data_path))
            root_directory = Path(repo_path)
            sizes.append(sum(f.stat().st_size for f in root_directory.glob(
                '**/*') if f.is_file())/10**6)
            shutil.rmtree(repo_path)
        (min_time, max_time, avg_time, std_time) = (
            min(times), max(times), mean(times), stdev(times))
        (min_size, max_size, avg_size, std_size) = (
            min(sizes), max(sizes), mean(sizes), stdev(sizes))

        ret = Results(min_time, avg_time, max_time,
                      std_time).to_row(self.name())
        ret.extend(["{:0.2f}".format(avg_size), "{:0.2f}".format(max_size)])
        return ret
