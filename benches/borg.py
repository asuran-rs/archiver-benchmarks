import benches.util as util
import shutil
import os


class Borg:
    """Benchmarking shim for borg"""
    i_name = "borg"

    def __init__(self, borg_path):
        self.borg_path = borg_path

    def name(self):
        return self.i_name

    def version(self):
        return util.capture_output([self.borg_path, "-V"])

    def new(self, path):
        """Creates a new archive at the given path"""
        os.environ["BORG_PASSPHRASE"] = "ASecurePassword"
        (code, time) = util.time_with_scrambled_stack(
            [self.borg_path, "init", "-e", "repokey-blake2", path]
        )
        if code != 0:
            raise Exception("Execution of borg failed")
        return time

    def snapshot_no_compression(self, repo_path, data_path):
        self.new(repo_path)
        repo_name = repo_path + "::repo"
        (code, time) = util.time_with_scrambled_stack(
            [self.borg_path, "create", "-C", "none", repo_name, data_path]
        )
        if code != 0:
            raise Exception("Execution of borg failed")
        return time

    def snapshot_zstd_1(self, repo_path, data_path):
        self.new(repo_path)
        repo_name = repo_path + "::repo"
        (code, time) = util.time_with_scrambled_stack(
            [self.borg_path, "create", "-C", "zstd,1", repo_name, data_path]
        )
        if code != 0:
            raise Exception("Execution of borg failed")
        return time
