Info
====

Each benchmark item was run 30 times, with environment variable randomization between each run, to provide some randomization of the stack starting point.

CPU is: Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz

Versions:
---------

| Archiver           | Version                                          |
|--------------------|--------------------------------------------------|
| asuran-cli (tokio) | Asuran-CLI 0.1.1-alpha.1-adf8e28 2020-04-27      |
| asuran-cli (smol)  | Asuran-CLI 0.1.1-alpha.1-18dd2de 2020-04-28      |
| borg               | borg 1.1.11                                      |
| restic             | restic 0.9.6 compiled with go1.14 on linux/amd64 |

Empty Repository Creation
=========================

| Archiver           | Min Time | Avg Time | Max Time | Std. Dev. |
|--------------------|----------|----------|----------|-----------|
| borg               | 0.41s    | 0.43s    | 0.69s    | 0.05s     |
| asuran-cli (tokio) | 0.58s    | 0.58s    | 0.60s    | 0.00s     |
| asuran-cli (smol)  | 0.58s    | 0.58s    | 0.60s    | 0.00s     |
| restic             | 2.56s    | 2.61s    | 2.72s    | 0.05s     |

Single Snapshot
===============

100MB English Text, single file
-------------------------------

Single snapshot, 100MB english text, no compression

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (tokio) | 0.66s    | 0.66s    | 0.78s    | 0.02s     | 100.42        | 100.42        |
| asuran-cli (smol)  | 0.66s    | 0.67s    | 0.67s    | 0.00s     | 100.42        | 100.43        |
| borg               | 0.98s    | 1.09s    | 1.13s    | 0.03s     | 100.05        | 100.05        |
| restic             | 1.09s    | 1.13s    | 1.16s    | 0.02s     | 100.02        | 100.03        |

Single snapshot, 100MB english text, ZStd-1

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (smol)  | 0.73s    | 0.73s    | 0.76s    | 0.01s     | 42.92         | 42.92         |
| asuran-cli (tokio) | 0.74s    | 0.75s    | 0.77s    | 0.01s     | 42.92         | 42.92         |
| borg               | 1.14s    | 1.26s    | 1.29s    | 0.02s     | 40.84         | 40.86         |

1GB english text, single file
-----------------------------

Single snapshot, 1GB english text, no compression

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (tokio) | 1.34s    | 1.39s    | 2.58s    | 0.23s     | 1004.11       | 1004.19       |
| asuran-cli (smol)  | 1.41s    | 1.42s    | 1.44s    | 0.01s     | 1004.10       | 1004.16       |
| restic             | 4.05s    | 4.15s    | 4.25s    | 0.05s     | 1000.18       | 1000.21       |
| borg               | 5.41s    | 6.42s    | 6.62s    | 0.20s     | 1000.10       | 1000.10       |

Single snapshot, 1GB english text, ZStd-1

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (smol)  | 1.95s    | 1.98s    | 2.01s    | 0.01s     | 380.25        | 380.25        |
| asuran-cli (tokio) | 2.00s    | 2.08s    | 2.18s    | 0.05s     | 380.25        | 380.25        |
| borg               | 7.09s    | 8.08s    | 8.26s    | 0.20s     | 358.08        | 358.13        |

Linux
-----

Original size 911.18MB

### No Compression

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (tokio) | 3.52s    | 3.62s    | 4.52s    | 0.18s     | 942.14        | 943.49        |
| asuran-cli (smol)  | 5.12s    | 5.24s    | 5.36s    | 0.06s     | 940.70        | 942.94        |
| restic             | 7.73s    | 7.99s    | 12.39s   | 0.83s     | 951.02        | 951.09        |
| borg               | 16.14s   | 27.31s   | 29.25s   | 2.19s     | 940.28        | 940.28        |

### ZStd-1

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (tokio) | 4.31s    | 4.44s    | 4.63s    | 0.07s     | 253.13        | 253.24        |
| asuran-cli (smol)  | 5.56s    | 5.66s    | 5.86s    | 0.06s     | 253.14        | 253.29        |
| borg               | 19.16s   | 31.11s   | 32.27s   | 2.28s     | 238.41        | 238.50        |

CentOS VM Image
---------------

Original size 4400.55MB

### No Compression

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (tokio) | 4.00s    | 4.21s    | 9.35s    | 0.97s     | 4006.61       | 4006.77       |
| asuran-cli (smol)  | 4.34s    | 4.37s    | 4.42s    | 0.02s     | 4010.75       | 4013.91       |
| restic             | 11.14s   | 11.33s   | 12.07s   | 0.15s     | 4254.88       | 4265.94       |
| borg               | 22.94s   | 26.75s   | 27.18s   | 0.74s     | 4348.34       | 4373.48       |

### ZStd-1

| Archiver           | Min Time | Avg Time | Max time | Std. Dev. | Avg Size (MB) | Max Size (MB) |
|--------------------|----------|----------|----------|-----------|---------------|---------------|
| asuran-cli (smol)  | 5.58s    | 5.63s    | 5.75s    | 0.03s     | 1664.10       | 1666.35       |
| asuran-cli (tokio) | 5.68s    | 6.07s    | 6.30s    | 0.12s     | 1666.64       | 1669.79       |
| borg               | 25.12s   | 29.13s   | 29.67s   | 0.79s     | 1657.83       | 1669.68       |
