import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="archiver-benches-thatonelutenist",
    version="0.0.1",
    author="Nathan McCarty",
    author_email="nathan@mccarty.io",
    description="Deduplicating archiver benchmarks",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://https://gitlab.com/asuran-rs/archiver-benchmarks",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD+Patent",
        "Operating System :: Linux",
    ],
    python_requires=">=3.8",
)
