#!/usr/bin/env python
from benches import *
from prettytable import PrettyTable
import prettytable
import cpuinfo

asuran = Asuran("/home/MCCARTY/nathan/.cargo/bin/asuran-cli")
asuran.i_name = "asuran-cli (tokio)"
asuran_smol = Asuran(
    '/home/MCCARTY/nathan/Projects/asuran/target/release/asuran-cli')
asuran_smol.i_name = "asuran-cli (smol)"
borg = Borg("/usr/bin/borg")
restic = Restic("/usr/bin/restic")

count = 30
shims = [asuran, asuran_smol, borg, restic]
harnesses = list(map(lambda x: Harness(count, x), shims))


def markdown_table():
    table = PrettyTable()
    table.hrules = prettytable.HEADER
    table.junction_char = '|'
    return table


def sort_key(row):
    string = row[2][:-1]
    return float(string)


print("# Info \n")

print("Each benchmark item was run {} times, with environment variable randomization".format(count))
print("between each run, to provide some randomization of the stack starting point.\n")
print("CPU is: " + cpuinfo.get_cpu_info()['brand'] + "\n")

print("## Versions: \n")

table = markdown_table()
table.field_names = ["Archiver", "Version"]
for harness in harnesses:
    table.add_row([harness.name(), harness.version()])
print(table, flush=True)
print("")

print("# Empty Repository Creation \n")

rows = []
for harness in harnesses:
    rows.append(harness.new())

table = markdown_table()
table.field_names = ["Archiver", "Min Time",
                     "Avg Time", "Max Time", "Std. Dev."]
rows.sort(key=sort_key)
for row in rows:
    table.add_row(row)

print(table, flush=True)
print("\n")

print("# Single Snapshot\n")
print("## 100MB English Text, single file\n")

print("Single snapshot, 100MB english text, no compression\n")

rows = []
enwiki8 = "/home/MCCARTY/nathan/Projects/archiver-benches/data/enwiki8"
for harness in harnesses:
    rows.append(harness.snapshot_no_compression(enwiki8))

table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    table.add_row(row)
print(table, flush=True)
print("")

print("Single snapshot, 100MB english text, ZStd-1\n")

rows = []
enwiki8 = "/home/MCCARTY/nathan/Projects/archiver-benches/data/enwiki8"
for harness in harnesses:
    rows.append(harness.snapshot_zstd_1(enwiki8))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    if float(row[5]) > 0:
        table.add_row(row)

print(table, flush=True)
print("")

print("## 1GB english text, single file \n")


print("Single snapshot, 1GB english text, no compression\n")

rows = []
enwiki9 = "/home/MCCARTY/nathan/Projects/archiver-benches/data/enwiki9"
for harness in harnesses:
    rows.append(harness.snapshot_no_compression(enwiki9))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    table.add_row(row)

print(table, flush=True)
print("")


print("Single snapshot, 1GB english text, ZStd-1\n")

rows = []
enwiki9 = "/home/MCCARTY/nathan/Projects/archiver-benches/data/enwiki9"
for harness in harnesses:
    rows.append(harness.snapshot_zstd_1(enwiki9))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    if float(row[5]) > 0:
        table.add_row(row)

print(table, flush=True)
print("")

print("## Linux\n")
linux = "/home/MCCARTY/nathan/Projects/archiver-benches/data/linux"
root_directory = Path(linux)
linux_size = sum(f.stat().st_size for f in root_directory.glob(
    '**/*') if f.is_file())/10**6
print("Original size {:0.2f}MB\n".format(linux_size))

print("### No Compression \n")

rows = []
for harness in harnesses:
    rows.append(harness.snapshot_no_compression(linux))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    table.add_row(row)

print(table, flush=True)
print("")

print("### ZStd-1 \n")

rows = []
for harness in harnesses:
    rows.append(harness.snapshot_zstd_1(linux))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    if float(row[5]) > 0:
        table.add_row(row)

print(table, flush=True)
print("")

print("## CentOS VM Image\n")
centos = "/home/MCCARTY/nathan/Projects/archiver-benches/data/centos/"
root_directory = Path(centos)
centos_size = sum(f.stat().st_size for f in root_directory.glob(
    '**/*') if f.is_file())/10**6
print("Original size {:0.2f}MB\n".format(centos_size))

print("### No Compression \n")

rows = []
for harness in harnesses:
    rows.append(harness.snapshot_no_compression(centos))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    table.add_row(row)

print(table, flush=True)
print("")

print("### ZStd-1 \n")

rows = []
for harness in harnesses:
    rows.append(harness.snapshot_zstd_1(centos))

rows.sort(key=lambda x: x[2])
table = markdown_table()
table.field_names = ["Archiver", "Min Time", "Avg Time",
                     "Max time", "Std. Dev.", "Avg Size (MB)", "Max Size (MB)"]
rows.sort(key=sort_key)
for row in rows:
    if float(row[5]) > 0:
        table.add_row(row)
print(table, flush=True)
