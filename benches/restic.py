import benches.util as util
import shutil
import os


class Restic:
    """Benchmarking shim for restic"""
    i_name = "restic"

    def __init__(self, restic_path):
        self.restic_path = restic_path

    def name(self):
        return self.i_name

    def version(self):
        return util.capture_output([self.restic_path, "version"])

    def new(self, path):
        """Creates a new path"""
        os.environ["RESTIC_PASSWORD"] = "ASecurePassword"
        (code, time) = util.time_with_scrambled_stack(
            [self.restic_path, "init", "-r", path]
        )
        if code != 0:
            raise Exception("Execution of restic failed")
        return time

    def snapshot_no_compression(self, repo_path, data_path):
        self.new(repo_path)
        (code, time) = util.time_with_scrambled_stack(
            [self.restic_path, "-r", repo_path, "backup", data_path]
        )
        if code != 0:
            raise Exception("Execution of restic failed")
        return time

    def snapshot_zstd_1(self, repo_path, data_path):
        (code, time) = util.time_with_scrambled_stack(
            ["mkdir", "-p", repo_path]
        )
        if code != 0:
            raise Exception("Execution of restic failed")
        return 0.0
