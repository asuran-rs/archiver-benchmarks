import benches.util as util
import shutil


class Asuran:
    """Benchmarking shim for asuran-cli"""
    i_name = "asuran-cli"

    def __init__(self, asuran_path):
        self.asuran_path = asuran_path

    def name(self):
        return self.i_name

    def version(self):
        return util.capture_output([self.asuran_path, "-V"])

    def new(self, path):
        """Creates a new archive at the given path, and returns the time it took to execute"""
        (code, time) = util.time_with_scrambled_stack(
            [self.asuran_path, "new", path, "-p", "ASecurePassword"])
        if code != 0:
            raise Exception("Execution of asuran-cli failed")
        return time

    def snapshot_no_compression(self, repo_path, snapshot_path):
        """Time to create a snapshot of the given directory"""
        self.new(repo_path)
        (code, time) = util.time_with_scrambled_stack(
            [self.asuran_path, "store", "-c", "None",
                repo_path, snapshot_path, "-p", "ASecurePassword"]
        )
        if code != 0:
            raise Exception("Execution of asuran-cli failed")
        return time

    def snapshot_zstd_1(self, repo_path, snapshot_path):
        """Time to create a snapshot of the given directory, with zstd,1"""
        self.new(repo_path)
        (code, time) = util.time_with_scrambled_stack(
            [self.asuran_path, "store", "-c", "ZStd", "-l", "1",
                repo_path, snapshot_path, "-p", "ASecurePassword"]
        )
        if code != 0:
            raise Exception("Execution of asuran-cli failed")
        return time
