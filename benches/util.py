import os
import random
import time
import subprocess


def stack_scramble():
    """Sets a random number of environment variables, `SCRAMBLE_N`, to random length strings

    Runtime of applications is oddly sensitive to exactly how things are laid out in memory, and
    environment variables are one of the first things loaded into an applications memory when it is
    started, "below the stack" as it were. By altering the environment variables and their contents,
    we can alter the amount of space in memory the environment takes up, shifting the starting
    location of the stack. By doing this before each measurement, and doing a number of
    measurements, we can take an average, and get better quality results, having controlled for the
    effects of memory layout somewhat.
    """
    MAX_SCRAMBLE_COUNT = 1
    MAX_SCRAMBLE_LENGTH = 16000

    # First, we unset all the scramble variables, to get a clean slate
    for i in range(0, MAX_SCRAMBLE_COUNT):
        env_var = 'SCRAMBLE_' + str(i)
        if env_var in os.environ:
            del os.environ[env_var]

    # Now we go through and set the scramble variables
    # for i in range(0, random.randrange(MAX_SCRAMBLE_COUNT)):
    env_var = 'SCRAMBLE_0'
    env_value = "".join(
        ['A' for i in range(0, random.randrange(MAX_SCRAMBLE_LENGTH))]) + 'H'
    os.environ[env_var] = env_value


def time_with_scrambled_stack(command):
    """Runs the command with a scrambled stack, and times its execution

    Will return (return_code, time) where time is in seconds
    """
    stack_scramble()
    start = time.perf_counter()
    process = subprocess.Popen(command,
                               stdout=subprocess.DEVNULL,
                               stderr=subprocess.DEVNULL,
                               stdin=subprocess.DEVNULL)
    return_code = process.wait()
    end = time.perf_counter()
    return (return_code, end-start)


def capture_output(command):
    process = subprocess.Popen(command,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.DEVNULL,
                               stdin=subprocess.DEVNULL)
    process.wait()
    out = process.stdout.readline().decode("utf-8").strip()
    return out
